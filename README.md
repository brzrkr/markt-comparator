# markt-comparator
A small (ugly) GUI program to manage a database of groceries.

The program supports entering, editing and deleting entries from the JSON
database and retrieving data about prices and markets where the entry is sold.

It is localised in Italian, English and German.

It was made for fun and practice and then abandoned to move on to different 
stuff. Don't judge me too much. ;)
